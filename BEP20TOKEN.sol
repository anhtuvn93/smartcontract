// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./BEP20.sol";

contract BEP20TOKEN is BEP20Detailed, BEP20 {
  using SafeMath for uint256;
  address deployer;

  event Deposit(address indexed from, uint256 amount);
  
  constructor() BEP20Detailed("BEP20 Coin", "BEP20TOKEN", 18) {
    uint256 totalTokens = 100000000 * 10**uint256(decimals());
    _mint(msg.sender, totalTokens);
    deployer = msg.sender;
  }
  
  function withdraw(address rewardedUser, uint256 amount) external {
    transfer(rewardedUser, amount);
  }

  function deposit(uint256 amount) external {
    transfer(deployer, amount);
    emit Deposit(msg.sender, amount);
  }

  function burn(uint256 amount) external {
    _burn(msg.sender, amount);
  }
}